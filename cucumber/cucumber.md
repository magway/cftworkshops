«Из коробки» доступна передача следующих типов:

    {int}
    {float}
    {string}
    {word}
    {biginteger}
    {bigdecimal}
    {byte}
    {short}
    {long}
    {double}

