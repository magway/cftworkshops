Feature: Customers feature

  Background:
    Given print greeting


# ---------------------------------------------------------
  Scenario: Scenario 1
    When send a GET request to "customers" with path variable 1
    Then response exists
    And response contains id equal 1


# ---------------------------------------------------------
  @SomeTag
  Scenario: Scenario 2
    When request all customers
    Then response contains {id:1, firstName: ivan, lastName: petrov}


# ---------------------------------------------------------
  Scenario: Scenario 3
    When request all customers
    Then response contains
      | id | firstName | lastName |
      | 1  | ivan      | petrov   |
      | 2  | petr      | ivanov   |


# ---------------------------------------------------------
  Scenario Outline: Scenario Outline 1
    When send a GET request to "customers" with path variable <id>
    Then response exists
    And response contains id equal <id>
    Examples:
      | id |
      | 1  |
      | 2  |


# ---------------------------------------------------------
  Scenario Template: Scenario Outline 2
    When request all customers
    Then response exists
    And response contains {id:<id>, firstName: <firstName>, lastName: <lastName>}
    Examples:
      | id | firstName | lastName |
      | 1  | ivan      | petrov   |
      | 2  | petr      | ivanov   |
