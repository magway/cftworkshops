package ru.cft.cucumber.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.Scenario;
import ru.cft.cucumber.TestContext;

public class FeatureHooks {

    @Before
    public void beforeScenario(Scenario scenario) {
        System.out.printf("before scenario <%s>%n", scenario.getName());
        TestContext.get().setData("scenario", scenario);
    }

    @Before(value = "@SomeTag", order = 10003)
    public void beforeScenarioWithSomeTag(Scenario scenario) {
        System.out.printf("before scenario <%s> with @SomeTag 3%n", scenario.getName());
    }

    @Before(value = "@SomeTag", order = 10002)
    public void beforeScenarioWithSomeTag2(Scenario scenario) {
        System.out.printf("before scenario <%s> with @SomeTag 2%n", scenario.getName());
    }

    @After
    public void afterScenario(Scenario scenario) {
        TestContext.reset();
    }

    @BeforeStep
    public void beforeStep(Scenario scenario) {
        System.out.println("before step");
    }

    @BeforeStep("@SomeTag")
    public void beforeStepWithSomeTag(Scenario scenario) {
        System.out.println("before step with @SomeTag");
    }

}
