package ru.cft.cucumber.config;

import io.cucumber.java.DataTableType;
import io.cucumber.java.ParameterType;
import org.springframework.lang.NonNull;
import ru.cft.cucumber.controler.dto.CustomerDto;

import java.util.Map;
import java.util.regex.Pattern;

public class CustomParameterTypes {

    private static final String customerDtoPatternString =
            "\\{id\\s*:\\s*(\\d+)\\s*,\\s*firstName\\s*:\\s*([\\S ]*\\S)\\s*,\\s*lastName\\s*:\\s*([\\S ]*\\S)\\s*}";

    private static final Pattern customerDtoPattern =
            Pattern.compile(customerDtoPatternString, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

    @ParameterType(customerDtoPatternString)
    public static CustomerDto customerDto(
            @NonNull String id,
            @NonNull String firstName,
            @NonNull String lastName) {
        /*Matcher matcher = customerDtoPattern.matcher(value);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid customer dto: " + value);
        }*/
        return new CustomerDto()
                .setId(Long.parseLong(id))
                .setFirstName(firstName)
                .setLastName(lastName);

    }

    @DataTableType
    public static CustomerDto customerDto(Map<String, String> row){
        return new CustomerDto()
                .setId(Long.parseLong(row.get("id")))
                .setFirstName(row.get("firstName"))
                .setLastName(row.get("lastName"));
    }
}
