package ru.cft.cucumber.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "ru.cft.cucumber")
@EnableAutoConfiguration
public class TestConfig {
}
