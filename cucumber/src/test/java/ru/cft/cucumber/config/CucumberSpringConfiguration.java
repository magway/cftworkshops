package ru.cft.cucumber.config;


import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import ru.cft.cucumber.Main;

import javax.annotation.PostConstruct;

@ActiveProfiles("test")
@CucumberContextConfiguration
@SpringBootTest(classes = {TestConfig.class, Main.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CucumberSpringConfiguration {

    @LocalServerPort
    private int port ;

    @PostConstruct
    public void postConstruct() {
        System.setProperty("port", String.valueOf(port));
    }

    protected String getUrl() {
        return "http://localhost:" + port;
    }

}
