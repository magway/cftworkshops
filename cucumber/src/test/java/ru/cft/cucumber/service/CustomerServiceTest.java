package ru.cft.cucumber.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.cft.cucumber.dao.CustomerRepository;
import ru.cft.cucumber.model.Customer;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerService customerService;

    @Test
    void findAll() {
        List<Customer> expected = Collections.singletonList(new Customer());
        Mockito.when(customerRepository.findAll()).thenReturn(expected);

        List<Customer> actual = StreamSupport.stream(customerService.findAll().spliterator(), false)
                .collect(Collectors.toList());
        Assertions.assertEquals(expected, actual);
        Mockito.verify(customerRepository).findAll();

    }

}