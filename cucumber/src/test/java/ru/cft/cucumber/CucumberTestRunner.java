package ru.cft.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        glue = {"ru.cft.cucumber.config", "ru.cft.cucumber.steps", "ru.cft.cucumber.hooks"},
        stepNotifications = true
)
public class CucumberTestRunner {
}
