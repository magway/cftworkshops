package ru.cft.cucumber.steps;

import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import ru.cft.cucumber.TestContext;
import ru.cft.cucumber.config.CucumberSpringConfiguration;
import ru.cft.cucumber.controler.dto.CustomerDto;

import java.util.Arrays;
import java.util.List;

import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

public class CustomerStepsDefinitions
        extends CucumberSpringConfiguration {

    @Autowired
    private TestRestTemplate restTemplate;

    @When("send a GET request to {string} with path variable {long}")
    public void theUserSendsAGETRequestTo(String path, long number) {
        String url = String.format("%s/%s/%d", getUrl(), path, number);
        TestContext.setResponse(restTemplate.getForEntity(url, CustomerDto.class));
    }

    @Then("^response exists$")
    public void responseExists() {
        HttpEntity<?> response = TestContext.getResponse();
        assertNotNull(response);
    }

    @Then("response contains id equal {long}")
    public void theResponseContainsIdEqualTo(long id) {
        CustomerDto customerDto = TestContext.getResponseBody(CustomerDto.class);
        assertNotNull(customerDto);
        assertEquals(id, customerDto.getId());
    }

    @Given("^print greeting$")
    public void printGreeting() {
        Scenario scenario = requireNonNull(TestContext.get().getData("scenario", Scenario.class));
        System.out.printf("***====== %s =======***%n", scenario.getName());
    }


    @When("^request all customers$")
    public void requestAllCustomersContains() {
        String url = String.format("%s/%s", getUrl(), "customers");
        TestContext.setResponse(restTemplate.getForEntity(url, CustomerDto[].class));
    }

    @Then("response contains {customerDto}")
    public void requestAllCustomersContains(CustomerDto customerDto) {
        CustomerDto[] customerDtos = requireNonNull(TestContext.getResponseBody(CustomerDto[].class));
        if (Arrays.stream(customerDtos).noneMatch(dto -> dto.equals(customerDto))) {
            fail(String.format("response %s\n doesn't contains\n %s", Arrays.toString(customerDtos), customerDto));
        }
    }

    @Then("response contains")
    public void requestAllCustomersContains(List<CustomerDto> list) {
        CustomerDto[] customerDtos = requireNonNull(TestContext.getResponseBody(CustomerDto[].class));
        for (CustomerDto customerDto : list) {
            if (Arrays.stream(customerDtos).noneMatch(dto -> dto.equals(customerDto))) {
                fail(String.format("response %s\n doesn't contains\n %s", Arrays.toString(customerDtos), customerDto));
            }

        }
    }
}
