package ru.cft.cucumber;

import org.springframework.http.HttpEntity;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;

public final class TestContext {

    private static final ThreadLocal<TestContext> CONTEXT = ThreadLocal.withInitial(TestContext::new);

    private final Map<String, Object> data = new HashMap<>();

    private TestContext() {
        System.out.println("create TestContext");
    }

    public static TestContext get() {
        return CONTEXT.get();
    }

    public static void reset() {
        CONTEXT.remove();
    }

    @Nullable
    public <Z> Z getData(@NonNull String key, @NonNull Class<Z> clazz)
            throws ClassCastException {
        return clazz.cast(data.get(key));
    }

    public void setData(@NonNull String key, @NonNull Object value) {
        data.put(key, value);
    }

    public static HttpEntity<?> getResponse() {
        return get().getData("response", HttpEntity.class);
    }

    public static <Z> Z getResponseBody(@NonNull Class<Z> clazz) {
        HttpEntity<?> response = getResponse();
        return isNull(response) ? null : clazz.cast(response.getBody());
    }

    public static void setResponse(HttpEntity<?> httpEntity) {
        get().setData("response", httpEntity);
    }
}
