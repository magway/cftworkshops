package ru.cft.cucumber.dao;

import org.springframework.data.repository.CrudRepository;
import ru.cft.cucumber.model.Customer;

public interface CustomerRepository
        extends CrudRepository<Customer, Long> {
}
