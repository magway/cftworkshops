package ru.cft.cucumber.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import ru.cft.cucumber.dao.CustomerRepository;
import ru.cft.cucumber.model.Customer;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @NonNull
    public Iterable<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Nullable
    public Customer findOne(Long id) {
        return customerRepository.findById(id).orElse(null);
    }

    public Long save(@NonNull Customer customer) {
        return customerRepository.save(customer).getId();
    }
}
