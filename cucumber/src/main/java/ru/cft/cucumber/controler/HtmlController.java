package ru.cft.cucumber.controler;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.cft.cucumber.controler.dto.CustomerDto;
import ru.cft.cucumber.model.Customer;
import ru.cft.cucumber.service.CustomerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Controller
public class HtmlController {

    private final CustomerService customerService;


    public HtmlController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/")
    public String blog(Model model) {

        List<CustomerDto> dtos = new ArrayList<>();
        customerService.findAll().forEach(customer -> dtos.add(new CustomerDto()
                .setId(customer.getId())
                .setFirstName(customer.getFirstName())
                .setLastName(customer.getLastName())));
        model.addAttribute("title", "some title");
        model.addAttribute("customers", dtos.toArray());

        return "blog";
    }

    @GetMapping("/{id}")
    public String customer(@PathVariable Long id, Model model) {
        Customer customer = customerService.findOne(id);
        if (Objects.isNull(customer)) {
            throw new IllegalArgumentException();
        }
        CustomerDto dto = new CustomerDto()
                .setId(customer.getId())
                .setFirstName(customer.getFirstName())
                .setLastName(customer.getLastName());
        model.addAttribute("customer", dto);
        model.addAttribute("title", "some title");

        return "customer";
    }

}
