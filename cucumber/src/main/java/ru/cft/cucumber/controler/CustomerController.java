package ru.cft.cucumber.controler;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cft.cucumber.controler.dto.CustomerDto;
import ru.cft.cucumber.model.Customer;
import ru.cft.cucumber.service.CustomerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController()
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("")
    public Long saveCustomer(@RequestBody CustomerDto customerDto) {
        Customer customer = new Customer()
                .setId(customerDto.getId())
                .setFirstName(customerDto.getFirstName())
                .setLastName(customerDto.getLastName());
        return customerService.save(customer);

    }

    @GetMapping("")
    public List<CustomerDto> getCustomers() {
        List<CustomerDto> dtos = new ArrayList<>();
        customerService.findAll().forEach(customer -> dtos.add(new CustomerDto()
                .setId(customer.getId())
                .setFirstName(customer.getFirstName())
                .setLastName(customer.getLastName())));
        return dtos;
    }

    @GetMapping("{id}")
    public CustomerDto getCustomer(@PathVariable Long id) {
        Customer customer = customerService.findOne(id);
        if (Objects.isNull(customer)) {
            throw new IllegalArgumentException();
        }
        return new CustomerDto()
                .setId(customer.getId())
                .setFirstName(customer.getFirstName())
                .setLastName(customer.getLastName());
    }


}
