package ru.cft.cucumber.controler.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CustomerDto {

    private Long id;

    private String firstName;

    private String lastName;

}
