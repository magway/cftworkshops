package ru.cft.cucumber.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "customer",
        uniqueConstraints = {
                @UniqueConstraint(name = "customer_name_unq", columnNames = {"first_name", "last_name"})
        })
@Getter
@Setter
@Accessors(chain = true)
public class Customer {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

}
